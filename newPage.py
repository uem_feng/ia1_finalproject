
from tkinter import *
from grafo import graph, get_adjacent_nodes_on_route_with_costs_speed_and_consumption as ganrwcsc, calculate_cost
class PaginaNova:
 
    def __init__(self, root, optimal_route, viagem):
        self.root = root
        self.optimal_route = optimal_route
        self.viagem = viagem
        self.tela()
        self.adicionar()
        self.calculate()
     
    def tela(self):
        self.root.title("Outra Página")
        self.root.geometry("700x500")
        self.root.configure(background='#BFDDF3')
 
    def adicionar(self):
        label_nova_pagina = Label(self.root, text="Bem Vindo a Moçambique!", font=("Times new roman", 18), bg='#BFDDF3')
        label_nova_pagina.pack(pady=20)

    def calculate(self):
        adjacent_nodes_on_route, total_toll_cost, total_average_speed, total_consumption = ganrwcsc(graph, self.optimal_route)
        ##########
        rota_label = Label(self.root, text=f"Rota Otp: {self.optimal_route}", font=("Times new roman", 12), bg='#BFDDF3')
        rota_label.pack()
        
        
        #########
        frame = Frame(self.root, bg='#BFDDF3')
        frame.pack(fill='both', expand=True)

        canvas = Canvas(frame, bg='#BFDDF3')
        canvas.pack(side=LEFT, fill='both', expand=True)

        scrollbar_y = Scrollbar(frame, orient="vertical", command=canvas.yview)
        scrollbar_y.pack(side=RIGHT, fill='y')

        canvas.config(yscrollcommand=scrollbar_y.set)

        interior_frame = Frame(canvas, bg='#BFDDF3')
        canvas.create_window((self.root.winfo_width() // 2, 0), window=interior_frame, anchor='n')

        for province, adjacent_provinces in adjacent_nodes_on_route.items():
            if adjacent_provinces:
                province_label = Label(interior_frame, text=f"Províncias vizinhas a {province} na rota:", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
                province_label.pack()

                for adjacent_province in adjacent_provinces:
                    adjacent_label = Label(interior_frame, text=f"- {adjacent_province['Localidade2']}:"
                                                          f"\n  - Piso: {adjacent_province['Piso']}"
                                                          f"\n  - Portagem: {adjacent_province['Portagem']}"
                                                          f"\n  - Velocidade Média: {adjacent_province['Velocidade_Média']}"
                                                          f"\n  - Consumo Médio: {adjacent_province['Consumo_Médio']}",
                                          font=("Times new roman", 10), bg='#BFDDF3', anchor='w', justify=LEFT)
                    adjacent_label.pack()

            else:
                no_adjacent_label = Label(interior_frame, text=f"{province} não possui províncias vizinhas na rota.", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
                no_adjacent_label.pack()

        total_speed_label = Label(interior_frame, text=f"Velocidade Média Total: {total_average_speed}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        total_speed_label.pack()

        total_consumption_label = Label(interior_frame, text=f"Consumo Médio Total: {total_consumption}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        total_consumption_label.pack()
        
        total2_consumption_label = Label(interior_frame, text=f"Custo total das portagens: {total_toll_cost}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        total2_consumption_label.pack()


        cost = calculate_cost(graph, self.optimal_route, self.viagem, print_partial=True)
        total_cost_label = Label(interior_frame, text=f"Custo total da rota: {cost}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        total_cost_label.pack()
        # for i in range(len(self.optimal_route) - 1):
        #     connection = next(neighbor for neighbor in graph[self.optimal_route[i]] if neighbor['Localidade2'] == self.optimal_route[i + 1])
        #     if self.viagem == "cost":
        #         cost += connection['Características']['Portagem']
        #         province_name = self.optimal_route[i]
        #         portagem = connection['Características']['Portagem']
        #         print(f"Valor da Portagem em {province_name}: {portagem}")
        #         total_cost_label2 = Label(interior_frame, text=f"Valor da Portagem em {province_name}: {portagem}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        #         total_cost_label2.pack()
        
        #     #print(f"Custo parcial em {self.optimal_route[i]}: {cost}")
        #     total_cost_label3 = Label(interior_frame, text=f"Custo parcial em {self.optimal_route[i]}: {cost}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        #     total_cost_label3.pack()
        # #print(f"Custo total da rota: {cost}")
        # total_cost_label4 = Label(interior_frame, text=f"Custo total da rota: {cost}", font=("Times new roman", 12), bg='#BFDDF3', anchor='w', justify=LEFT)
        # total_cost_label4.pack()

        interior_frame.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

if __name__ == "__main__":
    root = Tk()
   
    app = PaginaNova(root, self.optimal_route, self.viagem) 
    root.mainloop()
