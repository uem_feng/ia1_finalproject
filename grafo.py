from tkinter import *
from tkinter import messagebox


root = Tk()
root.withdraw()

def find_optimal_route(graph, start, end, criteria="duration"):
    visited = set()
    path = []
    optimal_path = None
    def dfs(node, current_path):
        nonlocal optimal_path

        visited.add(node)
        current_path.append(node)

        if node == end:
            if not optimal_path or calculate_cost(current_path, criteria) < calculate_cost(optimal_path, criteria):
                optimal_path = current_path
        else:
            for neighbor in graph.get(node, []):
                if neighbor['Localidade2'] not in visited:
                    dfs(neighbor['Localidade2'], list(current_path))

        visited.remove(node)
        current_path.pop()

    dfs(start, path)

    return optimal_path

#antigo 
def calculate_cost(graph, path, criteria, print_partial=False):
    cost = 0
    for i in range(len(path) - 1):
        connection = next(neighbor for neighbor in graph[path[i]] if neighbor['Localidade2'] == path[i + 1])
        if criteria == "distance":
            cost += connection['Distância']
        elif criteria == "duration":
            cost += connection['Distância'] / connection['Características']['Velocidade_Média']
        elif criteria == "cost":
            cost += connection['Características']['Portagem']
            province_name = path[i]
            portagem = connection['Características']['Portagem']
            if print_partial:
                print(f"Valor da Portagem em {province_name}: {portagem}")
        
        if print_partial:
            print(f"Custo parcial em {path[i]}: {cost}")

    if print_partial:
       # messagebox.showinfo('hello',f"Custo total da rota: {cost}")
        print(f"Custo total da rota: {cost}")

    return cost


graph = {
    'Matola': [
        {'Localidade1': 'Matola', 'Localidade2': 'Cidade de Maputo', 'Distância': 10, 'Características': {'Piso': 1, 'Portagem': 200, 'Velocidade_Média': 260, 'consumo_medio':50}},
        {'Localidade1': 'Matola', 'Localidade2': 'Gaza', 'Distância': 20, 'Características': {'Piso': 1, 'Portagem': 150, 'Velocidade_Média': 270, 'consumo_medio':502}}
    ],
    'Cidade de Maputo': [
        {'Localidade1': 'Cidade de Maputo', 'Localidade2': 'Matola', 'Distância': 10, 'Características': {'Piso': 1, 'Portagem': 159, 'Velocidade_Média': 60, 'consumo_medio':30}}
    ],
    'Gaza': [
        {'Localidade1': 'Gaza', 'Localidade2': 'Maputo', 'Distância': 20, 'Características': {'Piso': 2, 'Portagem': 300, 'Velocidade_Média': 70, 'consumo_medio':40}},
        {'Localidade1': 'Gaza', 'Localidade2': 'Inhambane', 'Distância': 30, 'Características': {'Piso': 2, 'Portagem': 349, 'Velocidade_Média': 50, 'consumo_medio':50}}
    ],
    'Inhambane': [
        {'Localidade1': 'Inhambane', 'Localidade2': 'Gaza', 'Distância': 30, 'Características': {'Piso': 3, 'Portagem': 499, 'Velocidade_Média': 50, 'consumo_medio':540}},
        {'Localidade1': 'Inhambane', 'Localidade2': 'Sofala', 'Distância': 40, 'Características': {'Piso': 3, 'Portagem': 333, 'Velocidade_Média': 60, 'consumo_medio':333}}
    ],
    'Sofala': [
        {'Localidade1': 'Sofala', 'Localidade2': 'Inhambane', 'Distância': 40, 'Características': {'Piso': 4, 'Portagem': 222, 'Velocidade_Média': 60, 'consumo_medio':522}},
        {'Localidade1': 'Sofala', 'Localidade2': 'Manica', 'Distância': 15, 'Características': {'Piso': 4, 'Portagem': 111, 'Velocidade_Média': 55, 'consumo_medio':53}}
    ],
    'Manica': [
        {'Localidade1': 'Manica', 'Localidade2': 'Sofala', 'Distância': 15, 'Características': {'Piso': 2, 'Portagem': 333, 'Velocidade_Média': 55, 'consumo_medio':504}},
        {'Localidade1': 'Manica', 'Localidade2': 'Tete', 'Distância': 25, 'Características': {'Piso': 5, 'Portagem': 0, 'Velocidade_Média': 65, 'consumo_medio':564}}
    ],
    'Tete': [
        {'Localidade1': 'Tete', 'Localidade2': 'Manica', 'Distância': 25, 'Características': {'Piso': 2, 'Portagem': 999, 'Velocidade_Média': 65, 'consumo_medio':30}},
        {'Localidade1': 'Tete', 'Localidade2': 'Zambézia', 'Distância': 35, 'Características': {'Piso': 5, 'Portagem': 0, 'Velocidade_Média': 50, 'consumo_medio':34}}
    ],
    'Zambézia': [
        {'Localidade1': 'Zambézia', 'Localidade2': 'Tete', 'Distância': 35, 'Características': {'Piso': 2, 'Portagem': 0, 'Velocidade_Média': 50, 'consumo_medio':50}},
        {'Localidade1': 'Zambézia', 'Localidade2': 'Nampula', 'Distância': 45, 'Características': {'Piso': 'Asfalto', 'Portagem': 0, 'Velocidade_Média': 60, 'consumo_medio':50}}
    ],
    'Nampula': [
        {'Localidade1': 'Nampula', 'Localidade2': 'Zambézia', 'Distância': 45, 'Características': {'Piso': 1, 'Portagem': 0, 'Velocidade_Média': 60, 'consumo_medio':50}},
        {'Localidade1': 'Nampula', 'Localidade2': 'Niassa', 'Distância': 50, 'Características': {'Piso': 5, 'Portagem': 0, 'Velocidade_Média': 55, 'consumo_medio':79}}
    ],
    'Niassa': [
        {'Localidade1': 'Niassa', 'Localidade2': 'Nampula', 'Distância': 50, 'Características': {'Piso': 3, 'Portagem': 0, 'Velocidade_Média': 55, 'consumo_medio':123}},
        {'Localidade1': 'Niassa', 'Localidade2': 'Cabo Delgado', 'Distância': 60, 'Características': {'Piso': 1, 'Portagem': 222, 'Velocidade_Média': 65, 'consumo_medio':50}}
    ],
    'Cabo Delgado': [
        {'Localidade1': 'Cabo Delgado', 'Localidade2': 'Niassa', 'Distância': 60, 'Características': {'Piso': 5, 'Portagem': 55, 'Velocidade_Média': 65, 'consumo_medio':50}}
    ]
}


#novo codigo 

def get_adjacent_nodes_on_route_with_costs_speed_and_consumption(graph, route):
    adjacent_nodes = {}
    total_toll_cost = 0  # Variável para armazenar o custo total das portagens
    total_average_speed = 0  # Variável para armazenar a velocidade média total
    total_consumption = 0  # Variável para armazenar o consumo médio total

    for province in route:
        adjacent_provinces = []
        
        if province in graph:
            for neighbor in graph[province]:
                adjacent_province = neighbor['Localidade2']
                if adjacent_province in route:
                    characteristics = neighbor['Características']
                    adjacent_provinces.append({
                        'Localidade2': adjacent_province,
                        'Piso': characteristics['Piso'],
                        'Portagem': characteristics['Portagem'],
                        'Velocidade_Média': characteristics['Velocidade_Média'],
                        'Consumo_Médio': characteristics.get('consumo_medio', 0),  # Adiciona o consumo médio ao total
                    })
                    total_toll_cost += characteristics['Portagem']  # Adiciona o custo de portagem ao total
                    total_average_speed += characteristics['Velocidade_Média']  # Adiciona a velocidade média ao total
                    total_consumption += characteristics.get('consumo_medio', 0)  # Adiciona o consumo médio ao total
        
        adjacent_nodes[province] = adjacent_provinces
    
    return adjacent_nodes, total_toll_cost, total_average_speed, total_consumption

# Exemplo de uso:
start_province = 'Gaza'
end_province = 'Tete'
selected_criteria = "distance"

print(f"Resultado da busca para o critério {selected_criteria}:")
optimal_route = find_optimal_route(graph, start_province, end_province, criteria=selected_criteria)

# if optimal_route:
#     print(f"Rota ótima completa: {optimal_route}")
    
#     adjacent_nodes_on_route, total_toll_cost, total_average_speed, total_consumption = get_adjacent_nodes_on_route_with_costs_speed_and_consumption(graph, optimal_route)

#     for province, adjacent_provinces in adjacent_nodes_on_route.items():
#         if adjacent_provinces:
#             print(f"Províncias vizinhas a {province} na rota:")
#             for adjacent_province in adjacent_provinces:
#                 print(f"- {adjacent_province['Localidade2']}:")
#                 print(f"  - Piso: {adjacent_province['Piso']}")
#                 print(f"  - Portagem: {adjacent_province['Portagem']}")
#                 print(f"  - Velocidade Média: {adjacent_province['Velocidade_Média']}")
#                 print(f"  - Consumo Médio: {adjacent_province['Consumo_Médio']}")
#         else:
#             print(f"{province} não possui províncias vizinhas na rota.")
    
#     print(f"Custo total das portagens: {total_toll_cost}")
#     print(f"Velocidade Média Total: {total_average_speed}")
#     print(f"Consumo Médio Total: {total_consumption}")
#     # Calculando o custo após encontrar a rota
#     #cost = calculate_cost(optimal_route, selected_criteria)
#     cost = calculate_cost(graph, optimal_route, selected_criteria, print_partial=True)
#     print(f"Custo total da rota: {cost}")

    
# else:
#     print("Rota não encontrada.")
