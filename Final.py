import tkinter as tk
from tkinter import *
from tkinter import ttk
from grafo import find_optimal_route, graph, get_adjacent_nodes_on_route_with_costs_speed_and_consumption as ganrwcsc, calculate_cost
from newPage import PaginaNova
from tkinter import messagebox

class app():

    def __init__(self):
        self.root = Tk()
        self.tela()
        self.titulo()
        self.escolher_viagem()
        # self.adicionar_botao_outra_pagina()
        self.root.mainloop()
        #
    def tela(self):
        self.root.title("Rotas")    
        self.root.geometry("400x500")
        self.root.configure(background='#BFDDF3')
        self.root.resizable(True, True)

    def titulo(self):
        texto_bem_vindo = Label(self.root, text="Bem vindo a Moçambique!", font=("Times new roman", 24), bg='#BFDDF3')
        texto_bem_vindo.pack(pady=20)

   
    def escolher_viagem(self):
        listopcoes = ["cost", "distance", "duration"]
        
        lb_viagem = Label(self.root, text="Escolha que tipo de viagem deseja fazer", font=("Times new roman", 14), bg='#BFDDF3')
        lb_viagem.pack(pady=(20, 10))
        
        self.cb_viagem = ttk.Combobox(self.root, values=listopcoes)
        self.cb_viagem.set('duration')
        self.cb_viagem.pack(pady=10)
        #cbq = ttk.Combobox(self.root, values=listopcoes)

        # origem 
        self.opcoes_origem = ["Cidade de Maputo", "Matola", "Gaza", "Inhambane", "Sofala", "Manica", "Tete", "Zambézia", "Nampula", "Niassa", "Cabo Delgado"]
        lb2_viagem= Label(self.root, text="Selecione a origem da sua viagem", font=("Times new roman", 14), bg='#BFDDF3')
        lb2_viagem.pack(pady=(20, 5))
       
        
        self.cb2_viagem = ttk.Combobox(self.root, values=self.opcoes_origem)
        self.cb2_viagem.set('Gaza')
        self.cb2_viagem.pack(pady=10)

        #destino
        self.opcoes_origem = ["Cidade de Maputo", "Matola", "Gaza", "Inhambane", "Sofala", "Manica", "Tete", "Zambézia", "Nampula", "Niassa", "Cabo Delgado"]
        lb3_viagem = Label(self.root, text="Selecione o destino da sua viagem", font=("Times new roman", 14), bg='#BFDDF3')
        lb3_viagem.pack(pady=(20, 5))
       
        
        self.cb3_viagem = ttk.Combobox(self.root, values=self.opcoes_origem)
        self.cb3_viagem.set('Niassa')
        self.cb3_viagem.pack(pady=10)
        #destino = self.cb_viagem.get()
    
    #botao
       
        btn_viagem = Button(self.root, text="Verificar percurso", command=self.viajar)
        btn_viagem.pack(pady=10, side="bottom", anchor="e")
       
        
    
      
    def viajar(self):
        viagem = self.cb_viagem.get()
        origem = self.cb2_viagem.get()
        destino = self.cb3_viagem.get()

        optimal_route = find_optimal_route(graph, origem, destino, viagem)
        
        if optimal_route:
            print(f"Rota ótima completa: {optimal_route}")
            self.root.destroy()
            nova_janela = Tk()
            PaginaNova(nova_janela, optimal_route, viagem)
            
        else:
          messagebox.showwarning('Rota invalida',"Rota não encontrada.")


if __name__ == "__main__":
    #app = app("","","")
    app = app()










